const products = [
	{
	        id: "6409c5c83386c56c5b69ef46",
	        name: "Cappuccino",
	        description: "A traditional Italian drink made with espresso and frothed milk",
	        price: 180,
	        isActive: true,
	    },
	    {
	        id: "6409ca51c129f991f43da0fd",
	        name: "Iced Cappuccino",
	        description: "A refreshing twist on the classic Italian drink made with chilled espresso and frothed milk",
	        price: 200,
	        isActive: false,
	    },
	    {
	        id: "6409cab1c129f991f43da100",
	        name: "Caramel Cappuccino",
	        description: "A sweet and indulgent treat made with espresso, frothed milk, and caramel syrup",
	        price: 220,
	        isActive: true,
	       
	    },
	    {
	        id: "6409cabfc129f991f43da103",
	        name: "Vanilla Cappuccino",
	        description: "A smooth and creamy drink made with espresso, frothed milk, and vanilla syrup",
	        price: 200,
	        isActive: true,
	       
	    },
	    {
	        id: "6409cad1c129f991f43da106",
	        name: "Hazelnut Cappuccino",
	        description: "A nutty and delicious drink made with espresso, frothed milk, and hazelnut syrup",
	        price: 220,
	        isActive: true,
	     
	    },
	    {
	        id: "6409cae0c129f991f43da109",
	        name: "Chocolate Cappuccino",
	        description: "A rich and decadent drink made with espresso, frothed milk, and chocolate syrup",
	        price: 220,
	        isActive: true,
	       
	    },
	    {
	        id: "6409caf3c129f991f43da10c",
	        name: "Coconut Cappuccino",
	        description: "A tropical twist on the classic Italian drink made with espresso, frothed coconut milk, and coconut flakes",
	        price: 220,
	        isActive: true,
	        
	    },
	    {
	        id: "6409cb01c129f991f43da10f",
	        name: "Chocolate Cinnamon Cappuccino",
	        description: "A decadent and spicy drink made with espresso, frothed milk, and chocolate-cinnamon syrup",
	        price: 175,
	        isActive: false,
	       
	    },
	    {
	        id: "6409cb12c129f991f43da112",
	        name: "Cinnamon Cappuccino",
	        description: "A spicy and warm drink made with espresso, frothed milk, and cinnamon syrup",
	        price: 260,
	        isActive: true,
	      
	    },
	    {
	        id: "6410662581391c71152b97b6",
	        name: "Coconut Cinnamon Cappuccino",
	        description: "A spicy and warm drink made with espresso, frothed milk, and cinnamon syrup with coconut milk",
	        price: 260,
	        isActive: true,
	       
	    }
]

export default products;
