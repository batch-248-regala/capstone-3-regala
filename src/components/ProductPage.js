import {useEffect, useState, useContext} from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';
import { Row, Col, Card, Button, Image } from 'react-bootstrap';
import './ProductCard.css';
import productImage1 from '../images/product-image1.png';
import productImage2 from '../images/product-image2.png';
import productImage3 from '../images/product-image3.png';

// productImage4, productImage5, productImage6, productImage7, productImage8, productImage9, productImage10

export default function ProductsPage() {

    const productImage = [ productImage1, productImage2, productImage3 ]

    const [products, setProducts] = useState([]);

    const {user} = useContext(UserContext);

    const [image, setImage] = useState([]); 

    useEffect(()=>{

      fetch(`http://localhost:3000/products/allProducts`)
      .then(res=>res.json())
      .then(data=>{

        setProducts(data)
      })
    },[])
    productImage.map((img)=>
      setImage(img));
   return (
    <Row className="mt-3 mb-3">
      <Col> 
        {
          products.map((product,index)=>
            <Card key={product._id} className="cardHighlight p-3">
              <Card.Img variant="top" src={image} style={{width: 300, height: 300, margin: "auto", display: "block"}} /> 
              <Card.Body>
                <Card.Title style={{ textTransform: 'uppercase', fontWeight: 'bold', fontSize: '1.5rem', marginBottom: '1rem' }}>{product.name}</Card.Title>
                <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>
                  {product.description}
                </Card.Text>
                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>
                  Php {product.price}
                </Card.Text>
                <Button className="banner-button" as={Link} to={`/products/${product._id}`}>
                  Details
                </Button>
              </Card.Body>
            </Card>
         )

        }
      </Col>
    </Row>
  )
  }
