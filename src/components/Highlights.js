import { Row, Col, Card } from "react-bootstrap";

export default function Highlights() {
return (
<Row className="mt-3 mb-3">
<Col xs={12} md={4}>
<Card className="cardHighlight p-3">
<Card.Body>
<Card.Title>Coffee of the Month</Card.Title>
<Card.Text>
Try our newest roast of the month, carefully selected by our
expert coffee tasters to deliver the best flavor and aroma.
</Card.Text>
</Card.Body>
</Card>
</Col>
<Col xs={12} md={4}>
<Card className="cardHighlight p-3">
<Card.Body>
<Card.Title>Happy Hour Deals</Card.Title>
<Card.Text>
Join us during our happy hour and enjoy a 20% discount on our
most popular coffee drinks and snacks.
</Card.Text>
</Card.Body>
</Card>
</Col>
<Col xs={12} md={4}>
<Card className="cardHighlight p-3">
<Card.Body>
<Card.Title>Join Our Loyalty Program</Card.Title>
<Card.Text>
Become a member of our loyalty program and get exclusive rewards,
such as free coffee on your birthday and discounts.
</Card.Text>
</Card.Body>
</Card>
</Col>
</Row>
);
}