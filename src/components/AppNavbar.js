import { useContext } from 'react';
import { Navbar, Nav, Container, Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import profileLogo from '../images/profilelogo.png';

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar bg="#5a3c22" variant="#5a3c22" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/" style={{ color: "#5a3c22" , fontWeight: "bold" }}>
          SKCUBRATS
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/" style={{ color: "#5a3c22"}}>
              Home
            </Nav.Link>
            <Nav.Link as={Link} to="/products/allProducts" style={{ color: "#5a3c22"}}>
              Products
            </Nav.Link>
          </Nav>
          <Nav className="ml-auto">
            {user.id !== null ? (
              <>
                <Nav.Link as={Link} to="/logout" style={{ color: "#5a3c22"}}>
                  Logout
                </Nav.Link>
                <Nav.Link as={Link} to="/admin">
                  <Image
                    src={profileLogo}
                    alt="Profile Logo"
                    width="30"
                    height="30"
                    roundedCircle
                  />
                </Nav.Link>
              </>
            ) : (
              <>
                <Nav.Link as={Link} to="/login" style={{ color: "#5a3c22"}}>
                  Login
                </Nav.Link>
                <Nav.Link as={Link} to="/register"style={{ color: "#5a3c22"}}>
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
