import { Link } from 'react-router-dom';
import { Row, Col, Card, Button, Image } from 'react-bootstrap';
import productImage from '../images/cup-of-cappuccino-free-png.webp';
import './ProductCard.css';

export default function ProductCard({productProp}) {

  const {name, description, price, _id} = productProp;

  return (
    <Row className="mt-3 mb-3">
      <Col> 
        <Card className="cardHighlight p-3">
          <Card.Img variant="top" src={productImage} style={{width: 300, height: 300, margin: "auto", display: "block"}} />
          <Card.Body>
            <Card.Title style={{ textTransform: 'uppercase', fontWeight: 'bold', fontSize: '1.5rem', marginBottom: '1rem' }}>{name}</Card.Title>
            <Card.Subtitle>Description</Card.Subtitle>
            <Card.Text>
              {description}
            </Card.Text>
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>
              Php {price}
            </Card.Text>
            <Button className="pc-button" as={Link} to={`/products/${_id}`}>
              Details
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  )
}