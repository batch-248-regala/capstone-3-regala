import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import './ProductCard.css';

export default function ProductView() {
  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const { productId } = useParams();

  const [productDetails, setProductDetails] = useState({
    name: "",
    description: "",
    price: "",
    quantity: ""
  });

  const handleOrder = async () => {
  try {
    const response = await fetch(`https://batch-248-regala-capstone-2.onrender.com/orders/createOrder`, {
      // http://localhost:3000/orders/createOrder
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        userId: user.id,
        products: [
          {
            productId: productId,
            quantity: 1 
          }
        ]
      })
    });
    const data = await response.json();
    console.log(data);

    if (data) {
      Swal.fire({
        title: "SUCCESS",
        icon: "success",
        text: "Successfully ordered the product!"
      });
      navigate("/products/allProducts");
    } else {
      Swal.fire({
        title: "Something went wrong",
        icon: "error",
        text: "Please try again."
      });
    }
  } catch (error) {
    console.error(error);
  }
};

  useEffect(() => {
    // http://localhost:3000/products/${productId}
    fetch(`https://batch-248-regala-capstone-2.onrender.com/products/${productId}`)
      .then(res => res.json())
      .then(data => {
        setProductDetails(data)
      })
  }, [setProductDetails, productId])

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card className="cardHighlight p-3">
            <Card.Body>
              <Card.Title>{productDetails.name}</Card.Title>
              <Card.Subtitle>Description</Card.Subtitle>
              <Card.Text>
                {productDetails.description}
              </Card.Text>
              <Card.Subtitle>Price</Card.Subtitle>
              <Card.Text>
                Php {productDetails.price}
              </Card.Text>

              {
                (user.id !== null) ?
                  <Button classNmae="banner-button" onClick={handleOrder}>Order</Button>
                  :
                  <Link className="btn btn-danger" to="/login">Log in to Order</Link>

              }

            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}
