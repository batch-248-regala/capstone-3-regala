import { useState, useEffect } from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
import ProductPage from './components/ProductPage';
import Dash from './components/Dash';
import AddProduct from './components/AddProduct';
import EditProduct from './components/EditProduct';
import Home from './pages/Home';
import Product from './pages/Product';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import './App.css';
import { UserProvider } from './UserContext'

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(()=>{
//`http://localhost:3000/users/singleUser`
    fetch(`https://batch-248-regala-capstone-2.onrender.com/users/singleUser`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data=>{
      console.log("data from app.js",data);
      if(typeof data._id !== "undefined"){

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  },[])
  console.log("user",user);
  return (
    
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavbar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/products/allProducts" element={<Product/>} />
            <Route path="/products/:productId" element={<ProductView/>}/>
            <Route path="/products" element={<ProductPage/>}/>
            <Route path="/login" element={<Login/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="*" element={<Error/>}/>
            <Route path="/admin" element={<Dash/>}/>
            <Route path="/AddProduct" element={<AddProduct/>} />
            <Route path="/EditProduct/:productId" element={<EditProduct/>} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>


  );
}

export default App;
