import {useEffect, useState, useContext} from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

export default function Products(){

	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext);

	useEffect(()=>{
		// http://localhost:3000/products/allProducts
		fetch(`https://batch-248-regala-capstone-2.onrender.com/products/allproducts`)
		.then(res=>res.json())
		.then(data=>{

			const productArr = (data.map(product=>{


				return(

					<ProductCard productProp={product} key={product._id}/>
				)
			}))
			setProducts(productArr)
		})
	},[products])



	return(
	<div className="text-center">
		<h1 style={{color: "#3a1c00"}}>Coffee Product List</h1>
		{products}
	</div>
)
}