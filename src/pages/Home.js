// import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import coffeeShopLogo from '../images/coffee-shop-logo.png';
import { Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';
// import {Button} from 'react-bootstrap';
import './Home.css';

const data = {
  title: "SKCUBRATS COFFEE SHOP",
  content: "Be our friend and experience the perfect blend",
  destination: "/products/allProducts",
  label: "Shop Now!"
}

function Banner({ data, styles }) {
  return (
    <div className="banner-container" style={styles}>
      <h1 className="banner-title">{data.title}</h1>
      <p className="banner-content">{data.content}</p>
      <Link to={data.destination} className="banner-button">
        {data.label}
      </Link>
    </div>
  );
}

const bannerStyles = {
  backgroundColor: "#f5f5f5",
  color: "#3a1c00",
  padding: "50px",
  borderRadius: "10px",
  boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
  fontFamily: "Poppins, sans-serif"
};

export default function Home() {
  return (
    <div className="container">
      <div className="text-center">
       <Image src={coffeeShopLogo} alt="Coffee Shop Logo" style={{ width: 490, height: 450 }} />
        <Banner data={data} styles={bannerStyles} />
      </div>
      <div className="d-flex justify-content-center">
        <Highlights />
      </div>
    </div>
  );
}
