import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");

  const [isActive, setIsActive] = useState(false);


  function authenticate(e){

    e.preventDefault();
    // http://localhost:3000/users/login
    fetch('https://batch-248-regala-capstone-2.onrender.com/users/login',{
      method:'POST',
      headers:{
        'Content-Type':'application/json'
      },
      body: JSON.stringify({
        email:email,
        password:password1
      })
    })
    .then(res=>res.json())
    .then(data=>{
      console.log("from authenticate function",data)
      console.log("data.accessToken",data.accessToken)

      if(typeof data.accessToken !== "undefined"){
        localStorage.setItem('token',data.accessToken)
        retrieveUserDetails(data.accessToken)
        console.log("retrieveUserDetails(data.access)",retrieveUserDetails)
        Swal.fire({
          title:"Login Successful!",
          icon:"success",
          text:"Welcome!"
        })


      }else{

        Swal.fire({
          title:"Authentication Unsuccessful!",
          icon:"error",
          text:"Check your credentials!"
        })

      }

    })

    const retrieveUserDetails = (token) =>{

      fetch('http://localhost:3000/users/singleUser',{
        headers:{
          Authorization: `Bearer ${token}`
        }
      })
      .then(res=>res.json())
      .then(data=>{
        console.log("from retrieveUserDetails",data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      })
    }

    setEmail("");
    setPassword1("");

  }


  useEffect(()=>{

    if(email !== "" && password1 !== ""){
      setIsActive(true)
    }else{
      setIsActive(false)
    }

  },[email, password1])


  return (

  (user.id !== null) ?
  <Navigate to="/"/>
  :
  <>
    <h1>Login</h1>

    <Form onSubmit={(e)=>authenticate(e)}>

        <Form.Group className="mb-3" controlId="userEmail">
          <Form.Label>Email Address</Form.Label>
          <Form.Control
            type="email"
            placeholder="Enter Email"
            value = {email}
            onChange = {e=>setEmail(e.target.value)}
            required/>
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            placeholder="Password"
            value = {password1}
            onChange = {e=>setPassword1(e.target.value)}
            required/>
        </Form.Group>

        {isActive ?

          <Button variant="success" type="submit" id="submitBtn">
            Submit
          </Button>

          :

          <Button variant="secondary" type="submit" id="submitBtn" disabled>
            Submit
          </Button>

        }

      </Form>
   </>
  )
}